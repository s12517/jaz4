package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import domain.Weather;

public final class showForecast_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("<title>Prognoza</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      services.WeatherApiService apiService = null;
      synchronized (application) {
        apiService = (services.WeatherApiService) _jspx_page_context.getAttribute("apiService", PageContext.APPLICATION_SCOPE);
        if (apiService == null){
          apiService = new services.WeatherApiService();
          _jspx_page_context.setAttribute("apiService", apiService, PageContext.APPLICATION_SCOPE);
        }
      }
      out.write('\r');
      out.write('\n');
      domain.Weather weather = null;
      synchronized (session) {
        weather = (domain.Weather) _jspx_page_context.getAttribute("weather", PageContext.SESSION_SCOPE);
        if (weather == null){
          weather = new domain.Weather();
          _jspx_page_context.setAttribute("weather", weather, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\r');
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("weather"), "city_name", request.getParameter("city_name"), request, "city_name", false);
      out.write("\r\n");
      out.write("\r\n");
      out.write("<h1>Prognoza dla: </h1>\r\n");
      out.write("<h2>");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((domain.Weather)_jspx_page_context.findAttribute("weather")).getCity_name())));
      out.write("</h2>\r\n");
      out.write(" \r\n");
      out.write("Zachmurzenie: ");
 out.println(apiService.main(weather.getCity_name()).getClouds_dt()); 
      out.write("%</br>\r\n");
      out.write("Temperatura: ");
 out.println(Math.round(apiService.main(weather.getCity_name()).getTemperature()*100d)/100d); 
      out.write("°C</br>\r\n");
      out.write("Ciśnienie: ");
 out.println(apiService.main(weather.getCity_name()).getPressure());
      out.write("hPa</br>\r\n");
      out.write("Prędkość Wiatru: ");
 out.println(apiService.main(weather.getCity_name()).getWind_speed()); 
      out.write(" m/s\r\n");
      out.write(" \r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}

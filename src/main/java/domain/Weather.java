package domain;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Weather {


private String weather_main;
private String weather_description; 
private Double temperature;
private Double pressure;
private Double wind_speed;
private int clouds_dt;
private String city_name;

public String getWeather_main() {
	return weather_main;
}
public void setWeather_main(String weather_main) {
	this.weather_main = weather_main;
}
public String getWeather_description() {
	return weather_description;
}
public void setWeather_description(String weather_description) {
	this.weather_description = weather_description;
}
public Double getTemperature() {
	return temperature;
}
public void setTemperature(Double temperature) {
	this.temperature = temperature;
}
public Double getPressure() {
	return pressure;
}
public void setPressure(Double pressure) {
	this.pressure = pressure;
}
public Double getWind_speed() {
	return wind_speed;
}
public void setWind_speed(Double wind_speed) {
	this.wind_speed = wind_speed;
}
public int getClouds_dt() {
	return clouds_dt;
}
public void setClouds_dt(int clouds_dt) {
	this.clouds_dt = clouds_dt;
}
public String getCity_name() {
	return city_name;
}
public void setCity_name(String city_name) {
	this.city_name = city_name;
} 


}

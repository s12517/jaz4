package services;

import java.io.IOException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import domain.Weather;


public class WeatherApiService {


    public static Weather main(String forecast1) {
    
    	final String URL =
                "http://api.openweathermap.org/data/2.5/weather?q="+forecast1+",pl&appid=cae9554ad79fdd64012e6f61b70cfc94";
     	
    	
    	Weather forecast = new Weather();
        String result = "";
         
        try {
            URL weather = new URL(URL);
 
            HttpURLConnection httpURLConnection = (HttpURLConnection) weather.openConnection();
 
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
 
                InputStreamReader reader =
                    new InputStreamReader(httpURLConnection.getInputStream());
                BufferedReader buffered =
                    new BufferedReader(reader, 8192);
                String line = null;
                while((line = buffered.readLine()) != null){
                    result = result + line;
                }
                  
                buffered.close();
                 	      
    	        JSONObject jsonObject = new JSONObject(result);
    	 
    	       JSONArray weatherArray = jsonObject.getJSONArray("weather");
    	        if(weatherArray.length() > 0){
    	            JSONObject mainWeather = weatherArray.getJSONObject(0);
    	            
    	            forecast.setWeather_main(mainWeather.getString("main"));         
    	            forecast.setWeather_description(mainWeather.getString("description")); 
    	          
    	        }

    	        JSONObject main = jsonObject.getJSONObject("main");
    	        
    	        	forecast.setTemperature(main.getDouble("temp")-273.15); 
    	        	forecast.setPressure(main.getDouble("pressure"));
    	        
    	        JSONObject wind = jsonObject.getJSONObject("wind");  
    	        	forecast.setWind_speed(wind.getDouble("speed"));

    	        JSONObject clouds = jsonObject.getJSONObject("clouds");
    	    
    	        	forecast.setClouds_dt(clouds.getInt("all")); 
    	      
 
            } else {
                System.out.println("Error! Nie pobrano danych!");
            }
 
        } catch (MalformedURLException x) {
            Logger.getLogger(WeatherApiService.class.getName()).log(Level.SEVERE, null, x);
        } catch (IOException x) {
            Logger.getLogger(WeatherApiService.class.getName()).log(Level.SEVERE, null, x);
        } catch (JSONException x) {
            Logger.getLogger(WeatherApiService.class.getName()).log(Level.SEVERE, null, x);
        }
        
        return forecast;
    }
	
}
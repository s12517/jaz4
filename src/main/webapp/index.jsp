<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Pogoda</title>
</head>
<body>

<jsp:useBean id="apiService" class="services.WeatherApiService" scope="application"/>
<jsp:useBean id="weather" class="domain.Weather" scope="session"/>


<form action="showForecast.jsp">
<label><b>Ktore miasto Cie interesuje?</b></label></br>
<label>Warsaw<input type="radio" value="Warszawa" name="city_name" id="city_name"/></label></br>
<label>Krakow<input type="radio" value="Krakow" name="city_name" id="city_name"/></label></br>
<label>Gdansk<input type="radio" value="Gdansk" name="city_name" id="city_name"/></label></br>
<label>Wroclaw<input type="radio" value="Wroclaw" name="city_name" id="city_name"/></label></br>
<label>Poznan<input type="radio" value="Poznan" name="city_name" id="city_name"/></label></br>
<label>Lodz<input type="radio" value="Lodz" name="city_name" id="city_name"/></label></br>
<label>Katowice<input type="radio" value="Katowice" name="city_name" id="city_name"/></label></br>

<input type = "submit" value="pokaz prognoze"/>
</form>
</body>
</html>
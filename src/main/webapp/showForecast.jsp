<%@page import="domain.Weather"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Prognoza</title>
</head>
<body>
<jsp:useBean id="apiService" class="services.WeatherApiService" scope="application"/>
<jsp:useBean id="weather" class="domain.Weather" scope="session"/>
<jsp:setProperty name="weather" property="city_name" param="city_name"/>

<h1>Prognoza dla: </h1>
<h2><jsp:getProperty name="weather" property="city_name"/></h2>
 
Zachmurzenie: <% out.println(apiService.main(weather.getCity_name()).getClouds_dt()); %>%</br>
Temperatura: <% out.println(Math.round(apiService.main(weather.getCity_name()).getTemperature()*100d)/100d); %>°C</br>
Ciśnienie: <% out.println(apiService.main(weather.getCity_name()).getPressure());%>hPa</br>
Prędkość Wiatru: <% out.println(apiService.main(weather.getCity_name()).getWind_speed()); %> m/s
 

</body>
</html>